//
//  CoreDataDelegate.swift
//  ProjetoMarvel
//
//  Created by C94276A on 10/12/21.
//


import CoreData

class CoreDataDelegate {
    
    public static var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "DataBase")
        container.loadPersistentStores(completionHandler: { (storeDeion, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    public static func saveContext() {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Você nao esta salvando no bd")
            }
        }
    }
}

