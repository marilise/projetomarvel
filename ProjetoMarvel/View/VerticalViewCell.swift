//
//  VerticalViewCell.swift
//  ProjetoMarvel
//
//  Created by Marilise Morona on 19/11/21.
//

import UIKit

class VerticalViewCell: UICollectionViewCell {
    
    var hero = [Results]()
    var customGradient = CustomGradient()
    
    @IBOutlet weak var uivImageHeroVertical: UIImageView!
    @IBOutlet weak var lblNameHero: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        customGradient.gradientImage(imageDefault: uivImageHeroVertical)
        
    }
    
    func setUpCell(hero: Results){
        lblNameHero.text = hero.name
        uivImageHeroVertical.layer.cornerRadius = 15
    }
}

