//
//  ImageDetailsViewCell.swift
//  ProjetoMarvel
//
//  Created by C94276A on 23/11/21.
//

import UIKit

class ImageDetailsViewCell: UITableViewCell {
 
    static let idCellImageDetailsViewCell = "CellImageDetailsViewCell"
    
    override init(frame: CGRect){
        super.init(frame: frame)
        contentView.backgroundColor = .blue

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

