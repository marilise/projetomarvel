//
//  AnimationViewCell.swift
//  ProjetoMarvel
//
//  Created by C94276A on 02/12/21.
//

import UIKit
import Lottie

class AnimationViewCell: UICollectionViewCell {

    @IBOutlet weak var uivAnimationView: AnimationView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    func configAnimation(){
     
        uivAnimationView?.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        uivAnimationView?.loopMode = .loop
        uivAnimationView?.play()
        print("add loading")
        
    }

}
