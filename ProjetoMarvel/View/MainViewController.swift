//
//  ViewController.swift
//  ProjetoMarvel
//
//  Created by Marilise Morona on 18/11/21.
//

import UIKit
import Lottie
import SwiftUI
import CoreData
import Firebase

class MainViewController: UIViewController, MainViewProtocol {
   
//    var uiSearchBar_: UISearchBar?
//    var nameStartsWith: String
    
    lazy var mainViewModel: MainViewModel = MainViewModel(mainViewProtocol: self, networkMonitorProtocol: NetworkMonitor(), apiProtocol: MarvelAPI(), coreDataProtocol: DataViewModel())
        
    var scrollView: UIScrollView = UIScrollView()
    var animationView = AnimationView(name: "loading")
    var firebaseManager = AnalyticsManager()
    
    @IBOutlet weak var clvHorizontalScroll: UICollectionView!
    @IBOutlet weak var uiSearchBar: UISearchBar!
    @IBOutlet weak var clvVerticalScroll: UICollectionView!
    @IBOutlet weak var txfSubtitleFeature: UILabel!
    @IBOutlet weak var lblSubtitleList: UILabel!
        
    override func viewDidLoad() {
            super.viewDidLoad()
    
        self.mainViewModel.networkMonitorProtocol.startMonitoring()
        configureNibCollections()
        mainViewModel.setupCaroussel()
        mainViewModel.setupHeroesList()
        insertSubtitle()
        configureDataSourceAndDelegate()
        configureLoadingAnimation()
        self.mainViewModel.heroesList = []
        
        firebaseManager.setPropertyOfTheUserID(heroFavorite: "3D-Man")

    }
    
    fileprivate func configureNibCollections(){
        let nib_h = UINib(nibName: "HorizontalViewCell", bundle: nil)
        clvHorizontalScroll.register(nib_h, forCellWithReuseIdentifier: "HorizontalViewCell")
        
        let nib_v = UINib(nibName: "VerticalViewCell", bundle: nil)
        clvVerticalScroll.register(nib_v, forCellWithReuseIdentifier: "VerticalViewCell")
    }
    
    fileprivate func configureDataSourceAndDelegate(){
        clvVerticalScroll.dataSource = self
        clvHorizontalScroll.dataSource = self
        clvVerticalScroll.delegate = self
        clvHorizontalScroll.delegate = self
        uiSearchBar.delegate = self
        scrollView.delegate = self
    }
    
    func reloadDataCaroussel(){
        self.clvHorizontalScroll.reloadData()
    }
    
    func reloadDataList(){
        self.clvVerticalScroll.reloadData()
    }

    //MARK: animation Methods
    func configureLoadingAnimation(){
        self.view.addSubview(animationView)
        animationView.loopMode = .loop
        animationView.frame = CGRect(x: self.view.bounds.width / 2 - 50, y: self.view.bounds.height - 100, width: 100, height: 100)
        animationView.isHidden = false
    }
    
    func startAnimation() {
        self.configureLoadingAnimation()
        animationView.isHidden = false
        animationView.play()
    }
    
    func hideAnimation(){
        configureLoadingAnimation()
        animationView.isHidden = true
        animationView.stop()
    }
    
    func insertSubtitle(){
        let textSubtitle = DataTable().subtitleOne
        let subtitleList = DataTable().subtitleTwo
        lblSubtitleList.text = subtitleList.uppercased()
        txfSubtitleFeature.text = textSubtitle.uppercased()
    }
    
    //MARK: viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //MARK: viewDidAppear
    override func viewDidAppear(_ animated: Bool){
        configureNavigationBar()
        
        firebaseManager.screenViewLoaded(AnalyticsParameterScreenName: "screenView", AnalyticsParameterScreenClass: "MainViewController")
        
        
    }
    private func configureNavigationBar(){
        let imageLogoBar = UIImageView(image: UIImage(named: "Logo2"))
        navigationItem.titleView = imageLogoBar
        imageLogoBar.isAccessibilityElement = true
        imageLogoBar.accessibilityLabel = "Marvel logo"
        
    }
    //chamado quando o view controller esta sendo apagado da memoria, oposto do didAppear
    override func viewDidDisappear(_ animated: Bool) {
        self.mainViewModel.networkMonitorProtocol.stopMonitoring()
        }
}
