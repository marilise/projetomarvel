//
//  DetailsImageViewCell.swift
//  ProjetoMarvel
//
//  Created by C94276A on 23/11/21.
//

import UIKit
import SnapKit

class DetailsImageViewCell : UITableViewCell {
    
    lazy var imageViewCell: UIImageView = {
        let image = UIImageView()
        image.frame = CGRect(x: 0, y: 0, width: 415, height: 415)
        image.contentMode = .scaleAspectFill
        
        return image
    }()
    var customGradient = CustomGradient()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?){
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
//        formatedImage()
        customGradient.gradientImage(imageDefault: imageViewCell)
    }
    
//    override func addSubview(_ view: UIView) {
//        super.addSubview(imageViewCell)
//
//    }
    
    func addSubview(){
        self.contentView.addSubview(self.imageViewCell)
    }
    
//    func formatedImage(){
//        imageViewCell.contentMode = .scaleAspectFit
//        imageViewCell.frame = CGRect(x: 0, y: 0, width: 300, height: 300)
//        self.imageViewCell.snp.makeConstraints { make in
//            make.top.equalTo(self).offset(10)
//            make.bottom.equalTo(self).offset(-10)
//            make.centerX.equalTo(self)
////            make.centerY.equalTo(self)
//        }

//   }
}
