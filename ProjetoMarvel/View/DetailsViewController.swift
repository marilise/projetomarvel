//
//  DetailsView.swift
//  ProjetoMarvel
//
//  Created by Marilise Morona on 22/11/21.
//

import UIKit
import Kingfisher
import Firebase

class DetailsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var firebaseManager = AnalyticsManager()
    
    lazy var detailsViewModel: DetailsViewModel = {
        return DetailsViewModel(detailsViewModel: self)
    }()

    let tableDetails = UITableView()
    
  
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(tableDetails)
        setUpConstraints()
        configureTableView()
        
    }
    
    private func configureTableView(){
        
        tableDetails.register(UITableViewCell.self, forCellReuseIdentifier: "CellDetails")
        tableDetails.dataSource = self
        tableDetails.translatesAutoresizingMaskIntoConstraints = true
        tableDetails.delegate = self
        tableDetails.backgroundColor = UIColor.backgroundColorBlack
        tableDetails.reloadData()
    }
    
    private func configureNavigationBar(){
        let imageLogoBar = UIImageView(image: UIImage(named: "Logo2"))
        navigationItem.titleView = imageLogoBar
        imageLogoBar.isAccessibilityElement = true
        imageLogoBar.accessibilityLabel = "Marvel Logo"
        imageLogoBar.accessibilityTraits = .image
    }
    
    //MARK: viewDidAppear
    override func viewDidAppear(_ animated: Bool){
        configureNavigationBar()
        
        firebaseManager.screenViewLoaded(AnalyticsParameterScreenName: "screenView", AnalyticsParameterScreenClass: "DetailsViewController")
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        tableDetails.frame = self.view.safeAreaLayoutGuide.layoutFrame
    }
    
    func setUpConstraints(){
        tableDetails.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        tableDetails.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        tableDetails.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        tableDetails.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
    }
}

