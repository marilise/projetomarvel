//
//  HorizontalViewCell.swift
//  ProjetoMarvel
//
//  Created by Marilise Morona on 19/11/21.
//

import UIKit

class HorizontalViewCell: UICollectionViewCell {
    
    var hero = [Results]()
    var customGradient = CustomGradient()
    
    @IBOutlet weak var uivImageHero: UIImageView!
    @IBOutlet weak var lblNameHero: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        customGradient.gradientImage(imageDefault: uivImageHero)
    }
    
    func setUpCell(hero: Results){
        lblNameHero.text = hero.name
        uivImageHero.layer.cornerRadius = 15
    }
}
