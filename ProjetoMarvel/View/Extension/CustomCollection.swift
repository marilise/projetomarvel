//
//  CustomCollection.swift
//  ProjetoMarvel
//
//  Created by C94276A on 26/11/21.
//

import UIKit
import Kingfisher
import Firebase


//MARK: Método Data Source
extension MainViewController : UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == clvHorizontalScroll {
            
            return self.mainViewModel.heroesCarroussel.count
        } else {
            
            return self.mainViewModel.heroesList.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == clvHorizontalScroll {
            
            let cell : HorizontalViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HorizontalViewCell", for: indexPath) as! HorizontalViewCell
            
            let character = self.mainViewModel.heroesCarroussel[indexPath.row]
            
            cell.lblNameHero.text = character.name
            
            let imagePath = character.thumbnail.path
            let imageExtension = character.thumbnail.ext
            let url = "\(String(describing: imagePath) )/standard_fantastic.\(String(describing: imageExtension))"
            if let heroUrlImage = URL(string: url){
                cell.uivImageHero.kf.setImage(with: heroUrlImage)
                cell.setUpCell(hero: character)
                
                return cell
            }
        }else {
            let cell : VerticalViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "VerticalViewCell", for: indexPath) as! VerticalViewCell
            
            let characterVertical = self.mainViewModel.heroesList[indexPath.row]
            
            cell.lblNameHero.text = characterVertical.name
            
            let imagePath = characterVertical.thumbnail.path
            let imageExtension = characterVertical.thumbnail.ext
            let url = "\(String(describing: imagePath) )/standard_fantastic.\(String(describing: imageExtension))"
            if let heroUrlImage = URL(string: url){
                cell.uivImageHeroVertical.kf.setImage(with: heroUrlImage)
                cell.setUpCell(hero: characterVertical)
                
                return cell
            }
        }
        
        return UICollectionViewCell()
    }
    //MARK: Método Delegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let details = DetailsViewController()
        
        
        if collectionView == clvHorizontalScroll {
           
            details.detailsViewModel.heroSelected = self.mainViewModel.heroesCarroussel[indexPath.row]
            
            let heroSelectedIntheCarousel = details.detailsViewModel.heroSelected?.name
            mainViewModel.firebaseManager.selectContentHeroInCollection(HeroSelectedInCollection: "Hero selected un the caroussel is \(heroSelectedIntheCarousel ?? "")" )
            
        }else {
            details.detailsViewModel.heroSelected = self.mainViewModel.heroesList[indexPath.row]
            
            let heroSelectedIntheList = details.detailsViewModel.heroSelected?.name
            mainViewModel.firebaseManager.selectContentHeroInCollection(HeroSelectedInCollection: "Hero selected un the caroussel is \(heroSelectedIntheList ?? "")" )
            
        }
        self.navigationController?.pushViewController(details, animated: true)
    }
}
