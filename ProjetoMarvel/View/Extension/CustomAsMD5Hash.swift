//
//  CustomAsMD5Hash.swift
//  ProjetoMarvel
//
//  Created by C94276A on 30/11/21.
//

import Security
import CommonCrypto
import CryptoKit


extension String {
    var asMD5Hash: String {
        return Insecure.MD5.hash(data: self.data(using: .utf8)!).map {
            String(format: "%02hhx", $0)
            
        }.joined()
    }
}
