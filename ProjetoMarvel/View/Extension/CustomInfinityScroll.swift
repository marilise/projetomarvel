//
//  CustomInfinityScroll.swift
//  ProjetoMarvel
//
//  Created by C94276A on 26/11/21.
//

import UIKit

//MARK: Método ScroolInifinito

extension MainViewController: UIScrollViewDelegate{

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let position = clvVerticalScroll.contentOffset.y
        let sizeCollection = clvVerticalScroll.contentSize.height
        
        if position > sizeCollection - clvVerticalScroll.frame.size.height && self.mainViewModel.apiProtocol.pagination == false {
            startAnimation()
            self.mainViewModel.scrollInfinito()
        }
        hideAnimation()
    }

//func scrollViewDidScroll(_ scrollView: UIScrollView) {
// let position = collectionVertical.contentOffset.y
//    let endPoint = collectionVertical.contentSize.height
//    if position > endPoint - scrollView.frame.size.height && api.pagination == false{
//        self.animation.hideLoading()
//        scroll()
//    }
//    if position == endPoint - scrollView.frame.size.height && api.pagination == true {
//        self.animation.showLoadingIcon()
//        
//    }
//
//
//
//    }
//
//
//
//}
}
