//
//  CustomAcessibilityCell.swift
//  ProjetoMarvel
//
//  Created by C94276A on 25/01/22.
//

import Foundation

extension MainViewController {
    func applyAccessibility(_ heroAccessibility: DataTable){
    
        txfSubtitleFeature.isAccessibilityElement = true
        txfSubtitleFeature.adjustsFontForContentSizeCategory = true
        txfSubtitleFeature.font = .preferredFont(forTextStyle: .body)
        txfSubtitleFeature.accessibilityLabel = heroAccessibility.subtitleOne
        txfSubtitleFeature.accessibilityTraits = .staticText
        
        clvHorizontalScroll.isAccessibilityElement = true
        clvHorizontalScroll.automaticallyAdjustsScrollIndicatorInsets = true
        clvHorizontalScroll.accessibilityTraits = .image
        
        lblSubtitleList.isAccessibilityElement = true
        lblSubtitleList.adjustsFontForContentSizeCategory = true
        lblSubtitleList.font = .preferredFont(forTextStyle: .body)
        lblSubtitleList.accessibilityLabel = heroAccessibility.subtitleTwo
        lblSubtitleList.accessibilityTraits = .staticText
        
        uiSearchBar.isAccessibilityElement = true
        uiSearchBar.searchTextField.adjustsFontForContentSizeCategory = true
        uiSearchBar.accessibilityLabel = "Search bar caracter, insert the first letter of the name Hero and press enter for search Heroes."
        uiSearchBar.accessibilityTraits = .searchField
        
        clvVerticalScroll.isAccessibilityElement = true
        clvVerticalScroll.automaticallyAdjustsScrollIndicatorInsets = true
        clvVerticalScroll.accessibilityTraits = .image
    }
}

extension HorizontalViewCell {
    func applyAccessibility(_ heroAccessibility: Results){

        uivImageHero.isAccessibilityElement = true
        uivImageHero.adjustsImageSizeForAccessibilityContentSizeCategory = true
        uivImageHero.accessibilityLabel = "Esta é uma foto do personagem \(heroAccessibility.name). Clique aqui para ter informações mais detalhadas sobre ele."
        uivImageHero.accessibilityTraits = .button

        lblNameHero.isAccessibilityElement = true
        lblNameHero.adjustsFontForContentSizeCategory = true
        lblNameHero.font = .preferredFont(forTextStyle: .body)
        lblNameHero.accessibilityLabel = heroAccessibility.name
        lblNameHero.accessibilityTraits = .staticText
    }
}

extension VerticalViewCell {
    func applyAccessibility(_ heroAccessibility: Results){

        uivImageHeroVertical.isAccessibilityElement = true
        uivImageHeroVertical.adjustsImageSizeForAccessibilityContentSizeCategory = true
        uivImageHeroVertical.accessibilityLabel = "Esta é uma foto do personagem \(heroAccessibility.name). Clique aqui para ter informações mais detalhadas sobre ele."
        uivImageHeroVertical.accessibilityTraits = .image

        lblNameHero.adjustsFontForContentSizeCategory = true
        lblNameHero.isAccessibilityElement = true
        lblNameHero.font = .preferredFont(forTextStyle: .body)
        lblNameHero.accessibilityLabel = heroAccessibility.name
        lblNameHero.accessibilityTraits = .staticText
    }
}



    

/** ANOTACOES IMPORTANTES **/
// nao ler os icones que nao tem juncao ou logica ao contexto
// o mesmo se aplica a imagens que nao trazem informacao
// modal ou alert
