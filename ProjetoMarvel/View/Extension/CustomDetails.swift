//
//  CustomDetails.swift
//  ProjetoMarvel
//
//  Created by C94276A on 06/01/22.
//

import UIKit
import Kingfisher


//MARK: Método Data Source
extension DetailsViewController {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellTableViewDetails = UITableViewCell(style: .subtitle, reuseIdentifier: "CellDetails")
        cellTableViewDetails.backgroundColor = UIColor.backgroundColorBlack
        
        switch indexPath.row {
            
        case 0:
            
            let cellImageDetails = DetailsImageViewCell()
            if let imagePath = self.detailsViewModel.heroSelected?.thumbnail.path,
               let imageExt = self.detailsViewModel.heroSelected?.thumbnail.ext {
            let url = "\(String(describing: imagePath))/standard_fantastic.\(String(describing: imageExt))"
            let heroUrlImage = URL(string: url)
                cellImageDetails.imageViewCell.kf.setImage(with: heroUrlImage)
                
            }
            
          return cellImageDetails
        case 1:
            cellTableViewDetails.textLabel?.text = self.detailsViewModel.heroSelected?.name
            cellTableViewDetails.textLabel?.font = UIFont.boldSystemFont(ofSize: 28.0)
            cellTableViewDetails.textLabel?.textColor = .white
            
        case 2:
            let subtitleDetails = DataTable().subtitleDetails.uppercased()
            cellTableViewDetails.textLabel?.text = subtitleDetails
            cellTableViewDetails.textLabel?.font = UIFont.boldSystemFont(ofSize: 15.0)
            cellTableViewDetails.textLabel?.textColor = .white
            if self.detailsViewModel.heroSelected?.description == "" {
                cellTableViewDetails.detailTextLabel?.text = self.detailsViewModel.descriptionDefault.textDefault
            }else {
                cellTableViewDetails.detailTextLabel?.text = self.detailsViewModel.heroSelected?.description
            }
            cellTableViewDetails.detailTextLabel?.numberOfLines = 0
            cellTableViewDetails.detailTextLabel?.font = UIFont.boldSystemFont(ofSize: 14.0)
            cellTableViewDetails.detailTextLabel?.textColor = .white
            cellTableViewDetails.detailTextLabel?.textAlignment = .justified
        default:
            return UITableViewCell()
        }
        return cellTableViewDetails
    }
// MARK: Método Delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return self.view.bounds.width + 20
        default:
            return UITableView.automaticDimension
        }
    }
}

//MARK: Método UINavigationController
extension UINavigationController{
    open override func viewWillLayoutSubviews() {
        navigationBar.topItem?.backButtonDisplayMode = .minimal
    }
}
