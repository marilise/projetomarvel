//
//  CustomGradient.swift
//  ProjetoMarvel
//
//  Created by C94276A on 07/01/22.
//

import UIKit

class CustomGradient {
    
    var gradient = false
    var imageDefault: UIImageView!
    
    func gradientImage(imageDefault: UIImageView){
        if gradient == false {
            let gradientLayer = CAGradientLayer()
            gradientLayer.colors = [UIColor.black.withAlphaComponent(0.0).cgColor,
                                    UIColor.black.withAlphaComponent(1.0).cgColor]
            gradientLayer.frame = imageDefault.bounds
            gradientLayer.locations = [0.0, 0.8, 0.1, 0.0]
            imageDefault.layer.insertSublayer(gradientLayer, at: 0)
            gradient = true
        }
    }
}
