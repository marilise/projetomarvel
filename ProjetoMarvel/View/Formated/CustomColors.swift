//
//  Colors.swift
//  ProjetoMarvel
//
//  Created by C94276A on 02/02/22.
//

import Foundation
import UIKit

extension UIColor {
    
    static let backgroundColorBlack = UIColor(
        red: 32/255,
        green: 32/255,
        blue: 32/255,
        alpha: 1.0)

}
