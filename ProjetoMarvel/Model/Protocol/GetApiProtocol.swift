//
//  Protocol.swift
//  ProjetoMarvel
//
//  Created by C94276A on 07/12/21.
//

import Foundation

protocol GetApiProtocol{
    
    var pagination : Bool { get }
  
    var offset : Int { get set }
    
    var nameStartsWith : String { get set }
    
    func url(_ nameStartsWith: String) -> String
        
    func getApi(url: String, completion: @escaping (Result<Characters, Error>) -> Void )

}
