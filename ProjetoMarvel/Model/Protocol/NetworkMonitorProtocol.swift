//
//  NetworkMonitorProtocol.swift
//  ProjetoMarvel
//
//  Created by C94276A on 18/01/22.
//

import Foundation


protocol NetworkMonitorProtocol {
    //read only
    var isReachable: Bool { get set }
    
    func startMonitoring()
    
    func stopMonitoring()
    
}
