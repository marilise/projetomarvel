//
//  CoreDataProtocol.swift
//  ProjetoMarvel
//
//  Created by C94276A on 18/01/22.
//

import Foundation

protocol CoreDataProtocol {
    
    func saveInDataBase(_ dataHeroes: [Results])
    
    func getHeroDataBase()-> [DataHero]
    
    static func removeAllSavedData()
}
