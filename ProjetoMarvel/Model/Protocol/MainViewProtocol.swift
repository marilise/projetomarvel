//
//  MainViewProtocol.swift
//  ProjetoMarvel
//
//  Created by C94276A on 14/01/22.
//

import Foundation
import Lottie
import UIKit

protocol MainViewProtocol : AnyObject {
    
//    var uiSearchBar_: UISearchBar? { get set }
//    var nameStartsWith: String { get set }
    
    func reloadDataCaroussel()
    func reloadDataList()
    func configureLoadingAnimation()
    func insertSubtitle()
    func startAnimation()
    func hideAnimation()
    
}
