//
//  FirebaseProtocol.swift
//  ProjetoMarvel
//
//  Created by C94276A on 31/01/22.
//

import Foundation

protocol FirebaseProtocol {
    
    func screenViewLoaded(AnalyticsParameterScreenName analyticsParameterScreenName: String, AnalyticsParameterScreenClass analyticsParameterScreenClass: String)
    
    func selectContentHeroInCollection(HeroSelectedInCollection heroSelectedInCollection: String)
    
    func selectContentSearchHero(StartNameWithSearchedUser startNameWithSearchedUser: String)
    
    func scrolledList(heroesCount: Int)
    
    func setPropertyOfTheUserID(heroFavorite: String)
    
}
