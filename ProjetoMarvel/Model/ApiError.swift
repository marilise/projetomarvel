//
//  ApiError.swift
//  ProjetoMarvel
//
//  Created by C94276A on 07/12/21.
//

import Foundation

enum ApiError: Error {
    case urlInvalid
    case apiInvalid
}
