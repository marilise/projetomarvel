//
//  BiographyText.swift
//  ProjetoMarvel
//
//  Created by C94276A on 30/11/21.
//

import Foundation


struct BiographyText {
    
    let textDefault : String = """

You'd love her. Very practical. Only a tiny bit sadistic. Some fuel cells were cracked during battle, but we figured out a way to reverse the ion charge to buy ourselves about 48 hours of time.  If we do this, how do we know it's gonna end any differently than it did before?  The coordinates for Vormir are laid in. All they have to do is not fall out.  Now, this may benefit your reality, but my new one…not so much. In this new branched Reality, without our chief weapon against the forces of darkness, our world will be overrun. Millions will suffer. So, tell me, Doctor, can your science prevent all that?  Oh, my eye. That's... you remember the Battle of Haroquin? When I got hit in the face with a broadsword?  His head was over there... His body over there... What was the point? I was too late. I was just standing there. Some idiot with an axe. Look, us sitting here staring at that thing is not gonna bring everybody back. I'm the strongest Avenger, okay? So this responsibility falls upon me. It's my duty.

Hey, you guys want mayo or mustard, or both?  Look, we go back, we get the stones before Thanos gets them... Thanos doesn't have the stones. Problem solved.  No, no, no. There's no other options. There's no do-overs. We're not going anywhere else. We have one particle left. Each. That's it, alright? We use that... Bye, bye. You're not going home.  You could not live with your own failure. And where did that bring you? Back to me. I thought by eliminating half of life, the other half would thrive. But you’ve shown me that’s impossible. And as long as there are those that remember what was, there will always be those that are unable to accept what can be. They will resist.

We need you. You're new blood. Bunch of tired old mules! Where to start? Umm... The Aether, first, is not a stone, someone called it a stone before. It's more of a... an angry sludge thing, so... someones gonna need to amend that. Here's an interesting story though, many years ago... My grandfather had to hide the stones from the Dark Elves...  I'm sorry. No offense, but you're a very earthly being. Okay? We're talking about space magic. And "can't" seems very definitive don't you think?  Everybody wants a happy ending, right? But it doesn't always roll that way. Maybe this time. I'm hoping if you play this back, it's in celebration. I hope families are reunited, I hope we get it back, and something like a normal version of the planet has been restored. If there ever was such a thing. God, what a world. Universe, now. If you told me ten years ago that we weren't alone, let alone, you know, to this extent, I mean, I wouldn't have been surprised. But come on, you know? The epic forces of darkness and light that have come into play. And, for better or worse, that's the reality Morgan's gonna have to find a way to grow up in.  It's time for me to be who I am rather than who I'm supposed to be. But you, you're a leader. That's who you are.

Hey, you guys want mayo or mustard, or both?  Kind of a step down from a from a golden palace for an Avenger highness and whatnot.  No. The same Nebula. From two different times. Set course for Morag. Scan the duplicate's memories. I want to see everything.  You could not live with your own failure. And where did that bring you? Back to me. I thought by eliminating half of life, the other half would thrive. But you’ve shown me that’s impossible. And as long as there are those that remember what was, there will always be those that are unable to accept what can be. They will resist.

Even if there's a small chance that we can undo this... I mean we owe it to everyone who's not in this room to try.  A dominion of death, at the very center of Celestial existence. It's where... Thanos murdered my sister.  Sire, the file appears entangled. It was a memory, but not hers. There's another consciousness sharing her network...another Nebula.  Oh, my eye. That's... you remember the Battle of Haroquin? When I got hit in the face with a broadsword?  His head was over there... His body over there... What was the point? I was too late. I was just standing there. Some idiot with an axe.

"""
}
