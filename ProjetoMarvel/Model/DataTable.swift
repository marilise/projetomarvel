//
//  DataTable.swift
//  ProjetoMarvel
//
//  Created by C94276A on 19/11/21.
//

import Foundation

struct DataTable {
    var title = "Marvel"
    var subtitleOne = "Featured characters"
    var subtitleTwo = "Marvel characters list"
    var subtitleDetails = "Biography"
    var searchBarText = "Search characters"
}
