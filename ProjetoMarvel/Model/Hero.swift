//
//  Hero.swift
//  ProjetoMarvel
//
//  Created by Marilise Morona on 22/11/21.
//

import Foundation
import UIKit


struct Characters: Codable {
    let data: Data
    
}

struct Data: Codable {
    let offset: Int
    let limit: Int
    let total: Int
    let count: Int
    let results: [Results]
    
}

struct Results: Codable {
    let id: Int
    let name: String
    let thumbnail: Thumbnail
    let description: String

}

struct Thumbnail: Codable{
    let path: String
    let ext: String
    
    enum CodingKeys: String, CodingKey {
        case path
        case ext = "extension"
    }
}
