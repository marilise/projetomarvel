//
//  DataViewModel.swift
//  ProjetoMarvel
//
//  Created by C94276A on 14/01/22.
//

import CoreData
import UIKit


class DataViewModel: CoreDataProtocol {
    
    var dataArray: [NSManagedObject] = []
    
    func saveInDataBase(_ dataHeroes: [Results]) {
        
        let context = CoreDataDelegate.persistentContainer.viewContext
        let currentHeroesDataBase = getHeroDataBase()
        let currentIds : [Int]
        
        currentIds = currentHeroesDataBase.map{ Int($0.dataId) }
        
        for hero in dataHeroes {
            if !currentIds.contains(hero.id){
                let dataHero = DataHero(context: context)
                dataHero.dataId = Int64(hero.id)
                dataHero.dataName = hero.name
                dataHero.dataImage = hero.thumbnail.path
                dataHero.dataImageExtension = hero.thumbnail.ext
                dataHero.dataDescription = hero.description
            }
            CoreDataDelegate.saveContext()
        }
        
    }
        
        func getHeroDataBase()-> [DataHero]{
            var data: [DataHero] = []
            do{
                data = try
                CoreDataDelegate.persistentContainer.viewContext.fetch(DataHero.fetchRequest())
            }catch {
                print("Erros in getHeroDataBase")
            }
            print("Contagem de heroes: \(data.count)")
            return data
        }
        
        static func removeAllSavedData() {
            let context =  CoreDataDelegate.persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "DataHero")
            let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
            do {
                try context.execute(batchDeleteRequest)
            } catch {
                print("Could not remove all.")
            }
        }
}
