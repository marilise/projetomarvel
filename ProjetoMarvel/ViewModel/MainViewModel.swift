//
//  MainViewModel.swift
//  ProjetoMarvel
//
//  Created by C94276A on 13/01/22.
//

import Foundation
import SwiftUI
import Lottie
import Firebase


class MainViewModel {
    
    weak var mainViewProtocol: MainViewProtocol?
    var networkMonitorProtocol: NetworkMonitorProtocol
    var apiProtocol: GetApiProtocol
    var coreDataProtocol: CoreDataProtocol
    
    var firebaseManager = AnalyticsManager()
    
    var heroes = [Results]()
    var heroesCarroussel = [Results]()
    var heroesList = [Results]()
    var heroesListBackup = [Results]()
    var filtered = [Results]()
    
    init(mainViewProtocol: MainViewProtocol, networkMonitorProtocol: NetworkMonitorProtocol, apiProtocol: GetApiProtocol, coreDataProtocol: CoreDataProtocol) {
        self.mainViewProtocol = mainViewProtocol
        self.networkMonitorProtocol = networkMonitorProtocol
        self.apiProtocol = apiProtocol
        self.coreDataProtocol = coreDataProtocol
        firebaseManager.selectContentHeroInCollection(HeroSelectedInCollection: "")
        firebaseManager.selectContentSearchHero(StartNameWithSearchedUser: "")
    }
    
    func setupCaroussel(){
        if self.networkMonitorProtocol.isReachable {
            loadCarousselApi()
        }else{
            loadCarousselCoreData()
        }
    }
    
    func setupHeroesList(){
        if self.networkMonitorProtocol.isReachable {
            loadApiArray()
        }else{
            loadCoreDataArray()
        }
    }
    
    func loadApiArray(){
        self.mainViewProtocol?.startAnimation()
        if apiProtocol.nameStartsWith != nil {
            self.apiProtocol.getApi(url: apiProtocol.url(apiProtocol.nameStartsWith)) { result in
                switch result {
                case .success(let characters):
                    let arrayHeroes = characters.data.results
                    self.heroesList = Array(arrayHeroes)
                    self.coreDataProtocol.saveInDataBase(arrayHeroes)
                    DispatchQueue.main.async {
                        self.mainViewProtocol?.reloadDataList()
                    }
                case .failure(let error):
                    print("not loading vertical")
                }
            }
            self.mainViewProtocol?.hideAnimation()
        }
    }
                                    
                                    
    func loadCarousselApi(){
        if apiProtocol.nameStartsWith != nil {
            self.apiProtocol.getApi(url: apiProtocol.url(apiProtocol.nameStartsWith)) { result in
                switch result {
                    case .success(let characters):
                        let arrayHeroes = characters.data.results
                        self.heroesCarroussel = Array(arrayHeroes[0...4])
                        self.coreDataProtocol.saveInDataBase(arrayHeroes)
                            DispatchQueue.main.async {
                                self.mainViewProtocol?.reloadDataCaroussel()
                            }
                    case .failure(let error):
                        print("not loading carousel")
                    }
            }
        }
    }
                                       
    func loadCoreDataArray(){
         let context = CoreDataDelegate.persistentContainer.viewContext
                let fetchRequest = DataHero.fetchRequest()
                var dataBase : [DataHero] = coreDataProtocol.getHeroDataBase()
                var resultsHero: [Results] = []
                    do {
                        dataBase = try! context.fetch(fetchRequest)
                        for hero in dataBase {
                            let newDataHero: Results = Results(id: Int(hero.dataId), name: (hero.dataName) ?? "",thumbnail: Thumbnail(path: (hero.dataImage) ?? "", ext: (hero.dataImageExtension) ?? ""), description: "")
                                resultsHero.append(newDataHero)
                            }
                            self.heroesList = Array(resultsHero)
                                DispatchQueue.main.async {
                                    self.mainViewProtocol?.reloadDataList()
                            }
                        }catch {
                            print("error loadDataBase")
                        }
                    }
                                       
    func loadCarousselCoreData(){
        let context = CoreDataDelegate.persistentContainer.viewContext
        let fetchRequest = DataHero.fetchRequest()
        var dataBase : [DataHero] = coreDataProtocol.getHeroDataBase()
        var resultsHero: [Results] = []
        do {
            dataBase = try! context.fetch(fetchRequest)
            for hero in dataBase {
                let newDataHero: Results = Results(id: Int(hero.dataId), name: (hero.dataName) ?? "",thumbnail: Thumbnail(path: (hero.dataImage) ?? "", ext: (hero.dataImageExtension) ?? ""), description: "")
                resultsHero.append(newDataHero)
            }
            self.heroesCarroussel = Array(resultsHero.prefix(5))
                DispatchQueue.main.async {
                    self.mainViewProtocol?.reloadDataCaroussel()
                }
            }catch {
                print("error loadDataBase")
            }
    }
                                       
    func scrollInfinito() {
        var offset = heroesList.count
            if apiProtocol.nameStartsWith != nil {
                self.apiProtocol.getApi(url: apiProtocol.url("")) { result in
                switch result {
                    case .success(let character):
                        let response = character.data.results
                        self.heroesList += response
                        self.coreDataProtocol.saveInDataBase(response)
                        offset+=20
                    case .failure(let error):
                        print(error)
                }
            }
                DispatchQueue.main.async {
                    self.mainViewProtocol?.reloadDataList()
                }
            }
    }
}
                                       
