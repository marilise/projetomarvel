//
//  DetailsViewModel.swift
//  ProjetoMarvel
//
//  Created by C94276A on 14/01/22.
//


import Kingfisher

class DetailsViewModel {
    
    var detailsViewController: DetailsViewController
    var heroSelected : Results?
    var descriptionDefault = BiographyText()
    var detailsImageViewCell = DetailsImageViewCell()

    init(detailsViewModel: DetailsViewController) {
        self.detailsViewController = detailsViewModel
    }
    
}

