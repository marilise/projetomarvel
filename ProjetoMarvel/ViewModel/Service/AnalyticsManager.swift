//
//  FirebaseManager.swift
//  ProjetoMarvel
//
//  Created by C94276A on 01/02/22.
//

import Foundation
import Firebase

class AnalyticsManager: FirebaseProtocol {
  
    func screenViewLoaded(AnalyticsParameterScreenName analyticsParameterScreenName: String, AnalyticsParameterScreenClass analyticsParameterScreenClass: String) {
        
        let screenView = AnalyticsEventScreenView
        Analytics.logEvent(screenView,
                                   parameters: [AnalyticsParameterScreenName: analyticsParameterScreenName,
                                               AnalyticsParameterScreenClass: analyticsParameterScreenClass])
    }
    
    func selectContentHeroInCollection(HeroSelectedInCollection heroSelectedInCollection: String) {
        
        Analytics.logEvent("select_content", parameters: [heroSelectedInCollection: String.self])
    
    }
    
    func selectContentSearchHero(StartNameWithSearchedUser startNameWithSearchedUser: String) {
        
        Analytics.logEvent("select_content", parameters: [startNameWithSearchedUser: String.self])
    }
   
    func setPropertyOfTheUserID(heroFavorite: String){
        Analytics.setUserID("UserID")
        Analytics.setUserProperty("", forName: "favorite_hero")
    }
    
    func scrolledList(heroesCount: Int){
        Analytics.logEvent("view_item_list", parameters: ["heroCount" : String.self])
    }
    
}
