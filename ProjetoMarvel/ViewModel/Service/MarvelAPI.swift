//
//  MarvelAPI.swift
//  ProjetoMarvel
//
//  Created by Marilise Morona on 26/11/21.
//

import Foundation
import Security
import CommonCrypto
import CryptoKit

let baseURL = "http://gateway.marvel.com"
let pathUrl = "v1/public/characters"
// Keys
let publicKey = Bundle.main.object(forInfoDictionaryKey: "publicKey") as! String
let privateKey = Bundle.main.object(forInfoDictionaryKey: "privateKey") as! String

let ts = Int(Date().timeIntervalSince1970)
let content = String(ts) + privateKey + publicKey
let hash = content.asMD5Hash

class MarvelAPI: GetApiProtocol {
   
    var nameStartsWith: String = ""
    var pagination = false
    var offset : Int = 0
    
    func url(_ nameStartsWith: String) -> String {
        
        var url : String
        if nameStartsWith.isEmpty {
            url = "\(baseURL)/\(pathUrl)?offset=\(offset)&ts=\(ts)&apikey=\(publicKey)&hash=\(hash)"
        } else {
            url = "\(baseURL)/\(pathUrl)?nameStartsWith=\(nameStartsWith)&offset=\(offset)&ts=\(ts)&apikey=\(publicKey)&hash=\(hash)"
        }
        return url
    }
    
    func getApi(url: String, completion: @escaping (Result<Characters, Error>) -> Void){
        if let url = URL(string: url) {
            print(url)
            self.pagination = true
            URLSession.shared.dataTask(with: url) {
                data, response, error in
                print(data)
                self.pagination = false
                if let data = data {
                    print(String(data: data, encoding: .utf8))
                    do {
                        let response = try JSONDecoder().decode(Characters.self, from: data)
                        self.offset += 20
                        print(response)
                        completion(.success(response))
                    } catch let error {
                        completion(.failure(ApiError.apiInvalid))
                    }
                }
            }.resume()
        }else {
            completion(.failure(ApiError.urlInvalid))
            print("Connection fail")
        }
    }
}
