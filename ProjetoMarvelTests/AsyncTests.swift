//
//  AsyncTests.swift
//  ProjetoMarvelTests
//
//  Created by C94276A on 07/12/21.
//

import XCTest
@testable import ProjetoMarvel

class AsyncTests: XCTestCase {
    
    var sut = MarvelAPI()
   
    
    func testDecoderJsonReturnsSuccessfully() throws {
        //Given
        let json = """
            {
        "id": 17492,
        "name": "Hero",
        "description": "This is a test description for Hero."
            }
        """
        //When
        let jsonData = json.data(using: .utf8)!
        let heroTest = try! JSONDecoder().decode(Results.self, from: jsonData)

        //Then
//       XCTAssertNil(heroTest.id)
        XCTAssertEqual(17492, heroTest.id)
        XCTAssertEqual("Hero", heroTest.name)
    }

    func testApiCallReturnsSuccessfullyWithEqualTwenty() {
        //Given
        let expectedResult = expectation(description: "Deu boa")
        
        // When
        sut.getApi(url: (sut.url())) { result in
            switch result {
                case .success(let characters):
                // Then
                    XCTAssertEqual(characters.data?.results?.count, 20)
                case .failure:
                    XCTFail("Fail")
            }
            expectedResult.fulfill()
        }
      waitForExpectations(timeout: 3.0)
    }
    
    func testApiCallReturnsUrlFailure() {
        // Given
        let expectedResult = expectation(description: "Deu ruim")
        let urlFake = "http://elephant-api.herokuapp.com/elephants"

        // When
        sut.getApi(url: urlFake) { result in
            switch result {
                case .success:
                // Then
                XCTFail("Fail")
                case .failure(let error):
                // Then
                
                XCTAssertFalse((error as? ApiError) == ApiError.apiInvalid)
            }
            expectedResult.fulfill()
        }
      waitForExpectations(timeout: 3.0)
    }
}
            
            
            
            
            
            
    
