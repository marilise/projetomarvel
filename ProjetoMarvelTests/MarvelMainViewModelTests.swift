//
//  MarvelMainViewModelTests.swift
//  ProjetoMarvelTests
//
//  Created by C94276A on 18/01/22.
//

import XCTest
@testable import ProjetoMarvel

class MarvelMainViewModelTests: XCTestCase {

    var sut : MainViewModel?
    
    override func setUp(){
        super.setUp()
        
    }

    func testDecoderJsonReturnsSuccessfully() throws {
        //Given
        
        sut = MainViewModel(mainViewProtocol: ViewControllerMock(), networkMonitorProtocol: NetworkMonitoringMock(), apiProtocol: GetApiMockCaseSuccess(), coreDataProtocol: CoreDataMock())
        //When
        let jsonData = json.data(using: .utf8)!
        let heroTest = try! JSONDecoder().decode(Characters.self, from: jsonData)
        //Then
        XCTAssertNotNil(heroTest, "Decoder sucessfully")

    }

    func testDecoderJsonReturnsfaILURE() throws {
        //Given
        sut = MainViewModel(mainViewProtocol: ViewControllerMock(), networkMonitorProtocol: NetworkMonitoringMock(), apiProtocol: GetApiMockCaseFailure(), coreDataProtocol: CoreDataMock())

        //When
        let jsonData = json.data(using: .utf8)!
        let heroTest = try! JSONDecoder().decode(Characters.self, from: jsonData)
        //Then
        XCTAssertNotNil(heroTest, "Decoder failure")

    }
    
    func testPopulatedCarousselWithApiReturnedSucess() {
        //Given
        let expectedResult = expectation(description: "Heroes loaded with succes")
        
        sut = MainViewModel(mainViewProtocol: ViewControllerMock(), networkMonitorProtocol: NetworkMonitoringMock(), apiProtocol: GetApiMockCaseSuccess(), coreDataProtocol: CoreDataMock())

        // When
        sut?.loadCarousselApi(nameStartsWith: "")
        //Then
        XCTAssertEqual(sut?.heroesCarroussel.count, 5)


        expectedResult.fulfill()
        waitForExpectations(timeout: 3.0)
    }

    func testPopulatedListWithApiReturnedSucess() {
        //Given
        let expectedResult = expectation(description: "Heroes loaded with succes")

        sut = MainViewModel(mainViewProtocol: ViewControllerMock(), networkMonitorProtocol: NetworkSucess(), apiProtocol: GetApiMockCaseSuccess(), coreDataProtocol: CoreDataMock())

        // When
        sut?.loadApiArray(nameStartsWith: "")
        //Then
        XCTAssertEqual(sut?.heroesList.count, 20)


        expectedResult.fulfill()
        waitForExpectations(timeout: 3.0)
    }

    func testPopulatedCarousselWithCoreDataReturnedSucess() {
        //Given
        let expectedResult = expectation(description: "Heroes loaded with succes")

        sut = MainViewModel(mainViewProtocol: ViewControllerMock(), networkMonitorProtocol: NetworkMonitoringMock(), apiProtocol: GetApiMockCaseFailure(), coreDataProtocol: CoreDataMock())

        // When
        sut?.loadCarousselCoreData()
        //Then
        XCTAssertEqual(sut?.heroesCarroussel.count, 5)


        expectedResult.fulfill()
        waitForExpectations(timeout: 3.0)
    }

    func testPopulatedListWithCoreDataReturnedSucess() {
        //Given
        let expectedResult = expectation(description: "Heroes loaded with succes")

        sut = MainViewModel(mainViewProtocol: ViewControllerMock(), networkMonitorProtocol: NetworkMonitoringMock(), apiProtocol: GetApiMockCaseFailure(), coreDataProtocol: CoreDataMock())

        // When
        sut?.loadCoreDataArray()
        //Then
        XCTAssertNotNil(sut?.heroesList.count)


        expectedResult.fulfill()
        waitForExpectations(timeout: 3.0)
    }

    func testPopulatedCarousselWithApiWhenConnectionIsSucess() {
        //Given
        let expectedResult = expectation(description: "Heroes loaded with succes")

        sut = MainViewModel(mainViewProtocol: ViewControllerMock(), networkMonitorProtocol: NetworkSucess(), apiProtocol: GetApiMockCaseSuccess(), coreDataProtocol: CoreDataMock())

        // When
        sut?.setupCaroussel()
        //Then
        XCTAssertNotNil(sut?.heroesCarroussel.count)


        expectedResult.fulfill()
        waitForExpectations(timeout: 3.0)
    }

    func testPopulatedListWithApiWhenConnectionIsSucess() {
        //Given
        let expectedResult = expectation(description: "Heroes loaded with succes")

        sut = MainViewModel(mainViewProtocol: ViewControllerMock(), networkMonitorProtocol: NetworkSucess(), apiProtocol: GetApiMockCaseSuccess(), coreDataProtocol: CoreDataMock())

        // When
        sut?.setupHeroesList()
        //Then
        XCTAssertNotNil(sut?.heroesList.count)


        expectedResult.fulfill()
        waitForExpectations(timeout: 3.0)
    }
    
    func testPopulatedCarousselWithApiWhenConnectionIsFailure() {
        //Given
        let expectedResult = expectation(description: "Heroes loaded with succes")
        
        sut = MainViewModel(mainViewProtocol: ViewControllerMock(), networkMonitorProtocol: NetworkFailure(), apiProtocol: GetApiMockCaseFailure(), coreDataProtocol: CoreDataMock())
        
        // When
        sut?.setupCaroussel()
        //Then
        XCTAssertNotNil(sut?.heroesCarroussel.count)
        
    
        expectedResult.fulfill()
        waitForExpectations(timeout: 3.0)
    }
    
    func testPopulatedListWithApiWhenConnectionIsFailure() {
        //Given
        let expectedResult = expectation(description: "Heroes loaded with succes")
        
        sut = MainViewModel(mainViewProtocol: ViewControllerMock(), networkMonitorProtocol: NetworkFailure(), apiProtocol: GetApiMockCaseFailure(), coreDataProtocol: CoreDataMock())
        
        // When
        sut?.setupHeroesList()
        //Then
        XCTAssertNotNil(sut?.heroesList.count)
        
    
        expectedResult.fulfill()
        waitForExpectations(timeout: 3.0)
    }
    
    func testReturnedScrollIsSucessfullyWithApi() {
        //Given
        let expectedResult = expectation(description: "Heroes loaded with succes")

        sut = MainViewModel(mainViewProtocol: ViewControllerMock(), networkMonitorProtocol: NetworkSucess(), apiProtocol: GetApiMockCaseScrollSuccess(), coreDataProtocol: CoreDataMock())

        // When
        sut?.scrollInfinito()
        //Then
        XCTAssertEqual(sut?.heroesList.count, 20)
        


        expectedResult.fulfill()
        waitForExpectations(timeout: 3.0)
    }
}
