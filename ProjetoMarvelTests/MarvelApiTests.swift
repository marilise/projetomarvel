//
//  AsyncTests.swift
//  ProjetoMarvelTests
//
//  Created by C94276A on 07/12/21.
//

import XCTest
@testable import ProjetoMarvel

class MarvelApiTests: XCTestCase {
    
    var sut = MarvelAPI()

    func testApiCallReturnsSuccessfullyWithEqualTwenty() {
        //Given
        let expectedResult = expectation(description: "Deu boa")
        let urlMarvel = sut.url("")

        // When
        sut.getApi(url: urlMarvel) { result in
            switch result {
                case .success(let characters):
                // Then
                XCTAssertEqual(characters.data.results.count, 20)
                case .failure:
                    XCTFail("Fail")
            }
            expectedResult.fulfill()
        }
        waitForExpectations(timeout: 3.0)
    }

    func testApiCallReturnsUrlFailure() {
        // Given
        let expectedResult = expectation(description: "Deu ruim")
        let urlFake = "http://elephant-api.herokuapp.com/elephants"

        // When
        sut.getApi(url: urlFake) { result in
            switch result {
                case .success:
                // Then
                XCTFail("Fail")
                case .failure(let error):
                // Then
                XCTAssert((error as? ApiError) == ApiError.apiInvalid)
            }
            expectedResult.fulfill()
        }
      waitForExpectations(timeout: 3.0)
    }
}
            
            
            
            
            
            
    
